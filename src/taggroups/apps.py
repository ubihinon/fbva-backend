from django.apps import AppConfig


class TagGroupConfig(AppConfig):
    name = 'taggroups'
