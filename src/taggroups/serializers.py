from rest_framework import serializers

from taggroups.models import TagGroup


class TagGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagGroup
        fields = ('id', 'name',)
