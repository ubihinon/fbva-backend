from django.conf.urls import url

from taggroups.views import TagGroupDeleteView, TagGroupUpdateView,\
    TagGroupDetailView, TagGroupCreateView, TagGroupListView

urlpatterns = [
    url(r'^(?P<pk>\d+)/delete/$', TagGroupDeleteView.as_view(), name='tag-group-delete'),
    url(r'^(?P<pk>\d+)/update/$', TagGroupUpdateView.as_view(), name='tag-group-update'),
    url(r'^(?P<pk>\d+)/$', TagGroupDetailView.as_view(), name='tag-group-detail'),
    url(r'^create/$', TagGroupCreateView.as_view(), name='tag-group-create'),
    url(r'^', TagGroupListView.as_view(), name='tag-groups'),
]
