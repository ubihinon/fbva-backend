from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView, RetrieveUpdateAPIView, DestroyAPIView

from rest_framework.permissions import IsAdminUser

from taggroups.models import TagGroup
from taggroups.serializers import TagGroupSerializer


class TagGroupListView(ListAPIView):
    serializer_class = TagGroupSerializer
    queryset = TagGroup.objects.all()
    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('name',)


class TagGroupDetailView(RetrieveAPIView):
    queryset = TagGroup.objects.all()
    serializer_class = TagGroupSerializer


class TagGroupCreateView(CreateAPIView):
    serializer_class = TagGroupSerializer
    queryset = TagGroup.objects.all()
    permission_classes = (IsAdminUser,)


class TagGroupUpdateView(RetrieveUpdateAPIView):
    serializer_class = TagGroupSerializer
    queryset = TagGroup.objects.all()
    permission_classes = (IsAdminUser,)


class TagGroupDeleteView(DestroyAPIView):
    serializer_class = TagGroupSerializer
    queryset = TagGroup.objects.all()
    permission_classes = (IsAdminUser,)
