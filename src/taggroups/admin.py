from django.contrib import admin

from taggroups.models import TagGroup


class TagGroupAdmin(admin.ModelAdmin):
    pass


admin.site.register(TagGroup, TagGroupAdmin)
