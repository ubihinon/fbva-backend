"""fbva URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from fbva import settings


api_urls = [
    path('languages/', include('languages.urls')),
    path('users/', include('users.urls')),
    path('intervals/', include('intervals.urls')),
    path('tags/', include('tags.urls')),
    path('settings/', include('settings.urls')),
    path('cards/', include('cards.urls')),
    path('images/', include('images.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
    url(r'^api/tag-groups/', include('taggroups.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
