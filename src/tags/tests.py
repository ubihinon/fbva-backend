import json

from django.urls import reverse

from users.models import User
from rest_framework.test import APITestCase


class TagCreateAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')

    def test_create_success(self):
        self.client.force_login(self.user)
        data = {
            "name": "tag1"
        }
        response = self.client.post(
            reverse('tags-list'),
            data,
        )
        content = json.loads(response.content)
        data['id'] = content.get('id', None)
        self.assertEqual(201, response.status_code)
        self.assertEqual(content.get('name', None), data['name'])
        self.assertEqual(content.get('tagGroupId', None), None)

    def test_create_fail_unauthorized(self):
        self.client.logout()
        data = {
            "name": "tag1"
        }
        response = self.client.post(
            reverse('tags-list'),
            data=data,
        )
        self.assertEqual(401, response.status_code)

    def test_create_fail_too_long_name(self):
        self.client.force_login(self.user)
        data = {
            "name": "tag123456789101112131415"
        }
        response = self.client.post(
            reverse('tags-list'),
            data=data,
        )
        self.assertEqual(400, response.status_code)


class TagListAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.client.force_login(self.user)
        self.tags = [
            {"name": "tag1"},
            {"name": "tag2"},
            {"name": "tag3"},
        ]
        response = self.client.post(reverse('tags-list'), data=self.tags[0])
        self.tags[0]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('tags-list'), data=self.tags[1])
        self.tags[1]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('tags-list'), data=self.tags[2])
        self.tags[2]['id'] = json.loads(response.content).get('id', None)

    def test_get_list(self):
        response = self.client.get(reverse('tags-list'))

        for i, value in enumerate(response.data):
            del value['tagGroupId']
            response.data[i] = value

        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(json.dumps(response.data), self.tags)

    def test_get_list_fail_unauthorized(self):
        self.client.logout()
        response = self.client.get(reverse('tags-list'))
        self.assertEqual(401, response.status_code)


class TagDetailAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.user2 = User.objects.create_user('user2', 'user2@mail.com', 'user2')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.user)
        self.tag = {
            "name": "tag1"
        }
        response = self.client.post(
            reverse('tags-list'),
            data=self.tag,
        )
        self.tag['id'] = json.loads(response.content).get('id', None)
        self.updated_tag = {
            "id": self.tag['id'],
            "name": "updated_tag1"
        }

    def test_get(self):
        response = self.client.get(reverse('tags-detail', kwargs={'pk': self.tag['id']}))
        self.assertEqual(200, response.status_code)

        del response.data['tagGroupId']

        self.assertJSONEqual(json.dumps(response.data), self.tag)

    def test_update_success(self):
        response = self.client.put(
            reverse('tags-detail', kwargs={'pk': self.tag['id']}),
            data=self.updated_tag
        )

        del response.data['tagGroupId']

        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(json.dumps(response.data), self.updated_tag)

    def test_update_fail(self):
        self.client.logout()
        self.client.login(user=self.user2)
        response = self.client.put(
            reverse('tags-detail', kwargs={'pk': self.tag['id']}),
            data=self.updated_tag
        )
        self.assertEqual(401, response.status_code)

    def test_update_fail_unauthorized(self):
        self.client.logout()
        response = self.client.put(
            reverse('tags-detail', kwargs={'pk': self.tag['id']}),
            data=self.updated_tag
        )
        self.assertEqual(401, response.status_code)

    def test_delete_success(self):
        response = self.client.delete(
            reverse('tags-detail', kwargs={'pk': self.tag['id']})
        )
        self.assertEqual(204, response.status_code)

    def test_delete_fail_another_user(self):
        self.client.logout()
        self.client.login(user=self.user2)
        response = self.client.delete(
            reverse('tags-detail', kwargs={'pk': self.tag['id']})
        )
        self.assertEqual(401, response.status_code)

    def test_delete_unauthorized(self):
        self.client.logout()
        response = self.client.delete(
            reverse('tags-detail', kwargs={'pk': self.tag['id']})
        )
        self.assertEqual(401, response.status_code)
