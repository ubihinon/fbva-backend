from rest_framework import serializers

from core.common.serializers import ObjectPrimaryKeyRelatedField
from taggroups.models import TagGroup
from tags.models import Tag


class TagSerializer(serializers.ModelSerializer):
    tagGroupId = ObjectPrimaryKeyRelatedField(source='tag_group_id',
                                              queryset=TagGroup.objects.all(),
                                              required=False,
                                              allow_null=True)

    class Meta:
        model = Tag
        fields = ('id', 'name', 'tagGroupId',)
