from django.core.validators import RegexValidator


class TagValidator(RegexValidator):
    regex = '^[0-9a-zA-Z_]+$'
    message = 'This field must contains only letters, numbers or _'
