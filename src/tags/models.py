from django.db import models

from taggroups.models import TagGroup
from tags.validators import TagValidator
from users.models import User


class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, validators=[TagValidator()])
    tag_group = models.ForeignKey(TagGroup, on_delete=models.DO_NOTHING,
                                  blank=True, null=True)
    creator = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name
