from rest_framework import serializers


class ObjectPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def to_internal_value(self, data):
        if isinstance(data, dict):
            obj_id = data['id']
        else:
            obj_id = data
        return self.queryset.get(pk=obj_id)
