from django.db import models

from tags.validators import TagValidator
from users.models import User


class Setting(models.Model):
    name = models.CharField(max_length=150, unique=True,
                            validators=[TagValidator()])
    default_value = models.CharField(max_length=30, default='')
    user = models.ManyToManyField(User, through='UserSetting')

    def __str__(self):
        return '{0}'.format(self.name)


class UserSetting(models.Model):
    setting = models.ForeignKey(Setting, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value = models.CharField(max_length=30)

    def __str__(self):
        return '[{0}] {1} = {2}'.format(self.user, self.setting.name, self.value)
