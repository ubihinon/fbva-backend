# Generated by Django 2.2 on 2019-04-25 01:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('settings', '0002_auto_20180508_0122'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='setting',
            options={},
        ),
        migrations.AlterModelOptions(
            name='usersetting',
            options={},
        ),
        migrations.AlterModelTable(
            name='setting',
            table=None,
        ),
        migrations.AlterModelTable(
            name='usersetting',
            table=None,
        ),
    ]
