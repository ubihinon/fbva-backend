import json

from django.urls import reverse

from users.models import User
from rest_framework.test import APITestCase


class SettingCreateAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.user)

    def test_create_success_as_superuser(self):
        self.client.force_login(self.superuser)
        data = {
            "name": "source_language",
            "defaultValue": "en"
        }
        response = self.client.post(
            reverse('settings-list'),
            data=data,
        )
        expected = {
            "id": json.loads(response.content).get('id', None),
            "name": "source_language",
            "defaultValue": "en",
            "user": []
        }
        self.assertEqual(201, response.status_code)
        self.assertJSONEqual(response.content, expected)

    def test_create_fail_as_user(self):
        self.client.logout()
        self.client.force_login(user=self.user)
        data = {
            "name": "source_language",
            "defaultValue": "en"
        }
        response = self.client.post(
            reverse('settings-list'),
            data=data,
        )
        self.assertEqual(403, response.status_code)

    def test_create_fail_unauthorized(self):
        self.client.logout()
        data = {
            "name": "source_language",
            "defaultValue": "en"
        }
        response = self.client.post(
            reverse('settings-list'),
            data=data,
        )
        self.assertEqual(401, response.status_code)


class UserSettingCreateAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.superuser)
        self.setting = {
            "name": "source_language",
            "defaultValue": "en",
            'users': []
        }
        response = self.client.post(
            reverse('settings-list'),
            data=self.setting,
        )
        self.setting['id'] = json.loads(response.content).get('id', None)

    def test_create_success_as_user(self):
        self.client.logout()
        self.client.force_login(user=self.user)
        data = {
            "setting": self.setting.get('id', None),
            "value": "fr"
        }
        response = self.client.post(
            reverse('user-settings-list'),
            data=data,
        )
        expected = {
            "name": "source_language",
            "value": "fr",
        }
        self.assertEqual(201, response.status_code)
        self.assertJSONEqual(response.content, expected)

    def test_create_fail_unauthorized(self):
        self.client.logout()
        data = {
            "setting": self.setting.get('id', None),
            "value": "fr"
        }
        response = self.client.post(
            reverse('user-settings-list'),
            data=data,
        )
        self.assertEqual(401, response.status_code)


class SettingListAPITestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.superuser)
        self.settings = [
            {
                "name": "source_language",
                "defaultValue": "en"
            },
            {
                "name": "target_language",
                "defaultValue": "ru"
            }
        ]
        response = self.client.post(reverse('settings-list'), data=self.settings[0])
        self.settings[0]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('settings-list'), data=self.settings[1])
        self.settings[1]['id'] = json.loads(response.content).get('id', None)

    def test_get_list(self):
        response = self.client.get(reverse('settings-list'))
        expected = [
            {
                "id": self.settings[0]['id'],
                "name": "source_language",
                "defaultValue": "en",
                "user": []
            },
            {
                "id": self.settings[1]['id'],
                "name": "target_language",
                "defaultValue": "ru",
                "user": []
            }
        ]
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, expected)


class UserSettingListAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.superuser)

        self.settings = [
            {
                "name": "source_language",
                "defaultValue": "en"
            },
            {
                "name": "target_language",
                "defaultValue": "ru"
            }
        ]
        response = self.client.post(reverse('settings-list'), data=self.settings[0])
        self.settings[0]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('settings-list'), data=self.settings[1])
        self.settings[1]['id'] = json.loads(response.content).get('id', None)
        self.client.logout()

    def test_get_list(self):
        self.client.force_login(user=self.user)
        expected = [
            {
                "name": "source_language",
                "defaultValue": "en",
                "value": "en"
            },
            {
                "name": "target_language",
                "defaultValue": "ru",
                "value": "ru"
            }
        ]
        response = self.client.get(
            reverse('user-settings-list')
        )
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, expected)

    def test_get_updated_list_success(self):
        self.client.logout()
        self.client.force_login(user=self.user)
        data = {
            "setting": self.settings[0].get('id', None),
            "value": "fr"
        }
        self.client.post(
            reverse('user-settings-list'),
            data=data,
        )
        expected = [
            {
                "name": "source_language",
                "defaultValue": "en",
                "value": "fr"
            },
            {
                "name": "target_language",
                "defaultValue": "ru",
                "value": "ru"
            }
        ]
        response = self.client.get(
            reverse('user-settings-list')
        )
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, expected)

    def test_get_updated_list_fail_unauthorized(self):
        self.client.logout()
        self.client.force_login(user=self.user)
        data = {
            "setting": self.settings[0].get('id', None),
            "value": "fr"
        }
        response = self.client.post(
            reverse('user-settings-list'),
            data=data,
        )
        self.assertEqual(201, response.status_code)
        self.client.logout()
        response = self.client.get(
            reverse('user-settings-list')
        )
        self.assertEqual(401, response.status_code)
