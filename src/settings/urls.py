from rest_framework.routers import DefaultRouter

from settings.views import UserSettingViewSet, SettingViewSet

router = DefaultRouter()
router.register(r'user-settings', UserSettingViewSet, base_name='user-settings')
router.register(r'', SettingViewSet, base_name='settings')

urlpatterns = router.urls
