from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.viewsets import GenericViewSet

from settings.models import Setting
from settings.serializers import SettingSerializer, UserSettingSerializer, \
    UserSettingCreateSerializer


class SettingViewSet(viewsets.ModelViewSet):
    queryset = Setting.objects.all()
    serializer_class = SettingSerializer

    def get_permissions(self):
        if self.action == 'list':
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = [IsAdminUser]
        return super().get_permissions()


class UserSettingViewSet(GenericViewSet, mixins.CreateModelMixin, mixins.ListModelMixin):
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == 'create':
            return UserSettingCreateSerializer
        return UserSettingSerializer

    def get_queryset(self):
        settings = Setting.objects.all()
        return settings

    def get_serializer_context(self):
        return {'user_id': self.request.user.id}

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        serializer.save()
