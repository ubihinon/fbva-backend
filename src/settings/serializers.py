from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from settings.models import Setting, UserSetting


class UserSettingCreateSerializer(serializers.ModelSerializer):
    setting = serializers.PrimaryKeyRelatedField(queryset=Setting.objects.all())

    def to_representation(self, instance):
        return {
            'name': instance.setting.name,
            'value': instance.value,
        }

    def to_internal_value(self, data):
        setting = None
        if 'setting' in data:
            setting = Setting.objects.get(id=int(data['setting']))
        elif 'name' in data:
            setting = Setting.objects.get(name=data['name'])

        return {
            'setting': setting,
            'value': data['value']
        }

    def create(self, validated_data):
        if isinstance(validated_data['user'], AnonymousUser):
            return
        user_setting, created = UserSetting.objects.update_or_create(
            validated_data,
            user=validated_data['user'],
            setting=validated_data['setting']
        )
        user_setting.save()
        return user_setting

    class Meta:
        model = UserSetting
        fields = ('setting', 'value',)


class UserSettingSerializer(serializers.ModelSerializer):
    defaultValue = serializers.CharField(source='default_value')

    def to_representation(self, instance):
        try:
            user_setting = UserSetting.objects.get(
                setting_id=instance.pk,
                user=self.context.get('user_id')
            )
            value = user_setting.value
        except ObjectDoesNotExist:
            value = instance.default_value
        return {
            'name': instance.name,
            'defaultValue': instance.default_value,
            'value': value,
        }

    class Meta:
        model = Setting
        fields = ('name', 'defaultValue', 'value')


class SettingSerializer(serializers.ModelSerializer):
    defaultValue = serializers.CharField(source='default_value')

    class Meta:
        model = Setting
        fields = ('id', 'name', 'defaultValue', 'user')
