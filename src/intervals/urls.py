from rest_framework.routers import DefaultRouter
from intervals.views import IntervalViewSet

router = DefaultRouter()
router.register(r'', IntervalViewSet, base_name='intervals')

urlpatterns = router.urls
