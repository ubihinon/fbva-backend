from django.contrib import admin

from intervals.models import Interval


class IntervalAdmin(admin.ModelAdmin):
    pass


admin.site.register(Interval, IntervalAdmin)
