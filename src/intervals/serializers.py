from intervals.models import Interval
from rest_framework import serializers


class IntervalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interval
        fields = ('id', 'order', 'interval',)
