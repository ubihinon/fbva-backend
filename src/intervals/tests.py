import json

from django.urls import reverse

from users.models import User
from rest_framework.test import APITestCase


class IntervalCreateAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.user)

    def test_create_success(self):
        self.client.force_login(self.superuser)

        data = {
            "order": 1,
            "interval": "00:00:30"
        }
        response = self.client.post(
            reverse('intervals-list'),
            data=data,
        )
        data['id'] = json.loads(response.content).get('id', None)

        self.assertEqual(201, response.status_code)
        self.assertJSONEqual(response.content, data)

    def test_create_fail(self):
        self.client.logout()

        data = {
            "order": 1,
            "interval": "00:00:30"
        }
        response = self.client.post(
            reverse('intervals-list'),
            data=data,
        )

        self.assertEqual(401, response.status_code)

    def test_create_unauthorized(self):
        self.client.logout()

        data = {
            "order": 1,
            "interval": "00:00:30"
        }
        response = self.client.post(
            reverse('intervals-list'),
            data=data,
        )

        self.assertEqual(401, response.status_code)


class IntervalListAPITestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.superuser)

        self.intervals = [
            {"order": 1, "interval": "00:00:30"},
            {"order": 2, "interval": "00:01:00"},
            {"order": 3, "interval": "00:02:00"}
        ]
        response = self.client.post(reverse('intervals-list'), data=self.intervals[0])
        self.intervals[0]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('intervals-list'), data=self.intervals[1])
        self.intervals[1]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('intervals-list'), data=self.intervals[2])
        self.intervals[2]['id'] = json.loads(response.content).get('id', None)

    def test_get_list(self):
        response = self.client.get(reverse('intervals-list'))

        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.intervals)

    def test_get_list_fail_unauthorized(self):
        self.client.logout()

        response = self.client.get(reverse('intervals-list'))
        self.assertEqual(401, response.status_code)


class IntervalDetailAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.superuser)

        self.interval = {
            "order": 1,
            "interval": "00:00:30"
        }
        response = self.client.post(
            reverse('intervals-list'),
            data=self.interval,
        )
        self.interval['id'] = json.loads(response.content).get('id', None)
        self.updated_interval = {
            "id": self.interval['id'],
            "order": 2,
            "interval": "00:01:00"
        }

    def test_get(self):
        response = self.client.get(reverse('intervals-detail', kwargs={'pk': self.interval['id']}))
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.interval)

    def test_update_success(self):
        response = self.client.put(
            reverse('intervals-detail', kwargs={'pk': self.interval['id']}),
            data=self.updated_interval
        )
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.updated_interval)

    def test_update_fail(self):
        self.client.logout()
        self.client.login(user=self.user)

        response = self.client.put(
            reverse('intervals-detail', kwargs={'pk': self.interval['id']}),
            data=self.updated_interval
        )

        self.assertEqual(401, response.status_code)

    def test_update_unauthorized(self):
        self.client.logout()

        response = self.client.put(
            reverse('intervals-detail', kwargs={'pk': self.interval['id']}),
            data=self.updated_interval
        )

        self.assertEqual(401, response.status_code)

    def test_delete_success(self):
        response = self.client.delete(
            reverse('intervals-detail', kwargs={'pk': self.interval['id']})
        )

        self.assertEqual(204, response.status_code)

    def test_delete_fail(self):
        self.client.logout()
        self.client.login(user=self.user)

        response = self.client.delete(
            reverse('intervals-detail', kwargs={'pk': self.interval['id']})
        )

        self.assertEqual(401, response.status_code)

    def test_delete_unauthorized(self):
        self.client.logout()

        response = self.client.delete(
            reverse('intervals-detail', kwargs={'pk': self.interval['id']})
        )

        self.assertEqual(401, response.status_code)
