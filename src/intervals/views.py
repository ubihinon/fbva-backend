from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from intervals.models import Interval
from intervals.serializers import IntervalSerializer


class IntervalViewSet(viewsets.ModelViewSet):
    queryset = Interval.objects.all()
    serializer_class = IntervalSerializer
    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('order',)

    def get_permissions(self):
        if self.action in ('list', 'retrieve'):
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = [IsAdminUser]
        return super().get_permissions()
