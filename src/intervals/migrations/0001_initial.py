# Generated by Django 2.0.2 on 2018-05-08 01:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Interval',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField()),
                ('interval', models.DurationField()),
            ],
            options={
                'db_table': 'interval',
                'managed': True,
            },
        ),
    ]
