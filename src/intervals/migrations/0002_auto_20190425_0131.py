# Generated by Django 2.2 on 2019-04-25 01:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('intervals', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='interval',
            options={},
        ),
        migrations.AlterModelTable(
            name='interval',
            table=None,
        ),
    ]
