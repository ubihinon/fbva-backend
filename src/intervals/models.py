from django.db import models


class Interval(models.Model):
    order = models.IntegerField()
    interval = models.DurationField()

    def __str__(self):
        return '{}: {}'.format(self.order, self.interval)
