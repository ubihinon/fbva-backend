from django.contrib.auth.models import Permission

from core.common.serializers import ObjectPrimaryKeyRelatedField
from languages.models import Language
from languages.serializers import LanguageSerializer
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, SerializerMethodField
from users.models import User


class UserSerializer(ModelSerializer):
    motherTongue = SerializerMethodField(source='mother_tongue', method_name='get_mother_tongue')
    profilePicture = serializers.CharField(source='profile_picture')
    isActive = serializers.BooleanField(source='is_active')
    lastLogin = serializers.DateTimeField(source='last_login')
    dateJoined = serializers.DateTimeField(source='date_joined')
    userPermissions = serializers.PrimaryKeyRelatedField(source='user_permissions', many=True,
                                                         queryset=Permission.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'email',
                  'profilePicture', 'sex', 'birthday', 'motherTongue',
                  'isActive', 'lastLogin', 'dateJoined', 'groups', 'userPermissions')

    def get_mother_tongue(self, obj):
        return LanguageSerializer(obj.mother_tongue).data


class UserUpdateSerializer(ModelSerializer):
    motherTongue = ObjectPrimaryKeyRelatedField(source='mother_tongue',
                                                queryset=Language.objects.all())

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            # 'profile_picture',
            'sex',
            'birthday',
            'motherTongue'
        )
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create(
            **validated_data
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserCreateSerializer(ModelSerializer):
    motherTongue = ObjectPrimaryKeyRelatedField(source='mother_tongue',
                                                queryset=Language.objects.all())

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'password',
            # 'profile_picture',
            'sex',
            'birthday',
            'motherTongue'
        )
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create(
            **validated_data
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class ChangePasswordSerializer(serializers.Serializer):
    oldPassword = serializers.CharField(required=True)
    newPassword = serializers.CharField(required=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass
