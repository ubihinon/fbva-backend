from django.db.models import Q
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from users.models import User
from users.permissions import IsOwnerOrReadOnly
from users.serializers import UserSerializer, UserUpdateSerializer, \
    ChangePasswordSerializer, UserCreateSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()

    def get_serializer_class(self):
        if self.action in ('update',):
            return UserUpdateSerializer
        elif self.action in ('create',):
            return UserCreateSerializer
        elif self.action in ('update_password',):
            return ChangePasswordSerializer
        return UserSerializer

    def get_permissions(self):
        if self.action in ('create', 'list', 'retrieve'):
            self.permission_classes = [AllowAny]
        else:
            self.permission_classes = [IsOwnerOrReadOnly]
        return super().get_permissions()

    def get_queryset(self):
        queryset = User.objects.all()
        search_parameter = self.request.GET.get('search')
        exact_parameter = self.request.GET.get('exact')
        search_by_parameter = self.request.GET.get('search-by')

        if search_parameter:
            if search_by_parameter == 'username' or search_by_parameter is None:
                if exact_parameter == 'false' or exact_parameter is None:
                    queryset = queryset.filter(
                        Q(username__contains=search_parameter)
                    ).order_by('username')
                else:
                    queryset = queryset.filter(
                        Q(username__exact=search_parameter)
                    )
            elif search_by_parameter == 'email':
                if exact_parameter == 'false' or exact_parameter is None:
                    queryset = queryset.filter(
                        Q(email__contains=search_parameter)
                    ).order_by('email')
                else:
                    queryset = queryset.filter(
                        Q(email__exact=search_parameter)
                    )
        return queryset

    @action(detail=True, url_path='update-password', methods=['PUT'])
    def update_password(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get("oldPassword")):
                return Response({"oldPassword": ["Wrong password."]},
                                status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("newPassword"))
            self.object.save()
            return Response("New password saved successfully!", status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
