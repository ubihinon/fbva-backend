import logging

from django.conf import settings
from django.core.management.base import BaseCommand
from ...models import User

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            logger.info(
                'Creating user for %s (%s)' % (settings.ADMIN_USERNAME, settings.ADMIN_EMAIL)
            )
            admin = User.objects.create_superuser(
                email=settings.ADMIN_EMAIL,
                username=settings.ADMIN_USERNAME,
                password=settings.ADMIN_INITIAL_PASSWORD
            )
            admin.is_active = True
            admin.is_admin = True
            admin.save()
        else:
            logger.info('Admin user can only be initialized if no users exist')
