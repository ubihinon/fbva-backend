import json

from django.urls import reverse

from languages.models import Language
from users.models import User
from rest_framework.test import APITestCase


class UserCreateAPITestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.superuser)
        self.language = {
            "code": "en",
            "name": "English"
        }
        response = self.client.post(
            reverse('languages-list'),
            data=self.language,
        )
        if json.loads(response.content).get('id', None):
            self.language['id'] = json.loads(response.content).get('id')

    def test_create_success(self):
        data = {
            "username": "user1",
            "password": "Developer1",
            "email": "user1@mail.com",
            "motherTongue": self.language['id'],
        }
        response = self.client.post(
            reverse('users-list'),
            data=data,
        )
        content = json.loads(response.content)
        self.assertEqual(201, response.status_code)
        self.assertEqual(content.get('username', None), data['username'])
        self.assertEqual(content.get('email', None), data['email'])
        self.assertEqual(content.get('sex', None), None)
        self.assertEqual(content.get('motherTongue', None), self.language['id'])

    def test_create_as_admin_success(self):
        self.client.login(user=self.superuser)
        data = {
            "username": "user2",
            "password": "Developer1",
            "email": "user2@mail.com",
            "motherTongue": self.language['id'],
        }
        response = self.client.post(
            reverse('users-list'),
            data=data,
        )
        content = json.loads(response.content)
        self.assertEqual(201, response.status_code)
        self.assertEqual(content.get('username', None), data['username'])
        self.assertEqual(content.get('email', None), data['email'])
        self.assertEqual(content.get('sex', None), None)
        self.assertEqual(content.get('motherTongue', None), self.language['id'])

    def test_create_as_user_success(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.client.login(user=self.user)
        data = {
            "username": "user3",
            "password": "Developer1",
            "email": "user3@mail.com",
            "motherTongue": self.language['id'],
        }
        response = self.client.post(
            reverse('users-list'),
            data=data,
        )
        content = json.loads(response.content)
        self.assertEqual(201, response.status_code)
        self.assertEqual(content.get('username', None), data['username'])
        self.assertEqual(content.get('email', None), data['email'])
        self.assertEqual(content.get('sex', None), None)
        self.assertEqual(content.get('motherTongue', None), self.language['id'])

    def test_obtain_token_auth_success(self):
        data = {
            "email": 'admin@mail.com',
            "password": "admin",
        }

        response = self.client.post(
            reverse('token-auth'),
            data=data,
        )

        content = json.loads(response.content)
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(content.get('token', None)), 195)


class UserListAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.superuser)
        self.language = {
            "code": "en",
            "name": "English"
        }
        response = self.client.post(
            reverse('languages-list'),
            data=self.language,
        )
        if json.loads(response.content).get('id', None):
            self.language['id'] = json.loads(response.content).get('id', None)
            self.users = [
                {
                    "username": "user4",
                    "password": "Developer1",
                    "email": "user4@mail.com",
                    "motherTongue": self.language['id'],
                },
                {
                    "username": "user2",
                    "password": "Developer1",
                    "email": "user2@mail.com",
                    "motherTongue": self.language['id'],
                },
                {
                    "username": "user3",
                    "password": "Developer1",
                    "email": "user3@mail.com",
                    "motherTongue": self.language['id'],
                }
            ]
            response1 = self.client.post(reverse('users-list'), data=self.users[0])
            self.users[0] = json.loads(response1.content)
            response2 = self.client.post(reverse('users-list'), data=self.users[1])
            self.users[1] = json.loads(response2.content)
            response3 = self.client.post(reverse('users-list'), data=self.users[2])
            self.users[2] = json.loads(response3.content)

    def test_get_list(self):
        response = self.client.get(reverse('users-list'))
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content)
        self.assertGreaterEqual(len(content), len(self.users))


class UserDetailAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'Developer1')
        self.user.sex = 'M'
        self.user2 = User.objects.create_user('user2', 'user2@mail.com', 'Developer1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.superuser)
        self.language = {
            "code": "en",
            "name": "English"
        }

        response = self.client.post(
            reverse('languages-list'),
            data=self.language,
        )

        if json.loads(response.content).get('id', None):
            self.language['id'] = json.loads(response.content).get('id', None)
        else:
            response2 = self.client.get(reverse('languages-list'))
            self.language['id'] = json.loads(response2.content).get('id', None)
        self.user.mother_tongue = Language(
            id=self.language['id'],
            code=self.language['code'],
            name=self.language['name']
        )

    def test_get(self):
        response = self.client.get(reverse('users-detail', kwargs={'pk': self.user.id}))
        content = json.loads(response.content)
        self.assertEqual(200, response.status_code)
        self.assertEqual(self.user.id, content['id'])
        self.assertEqual(self.user.username, content['username'])
        self.assertEqual(self.user.email, content['email'])
        self.assertEqual(self.user.profile_picture, content['profilePicture'])
        self.assertEqual(self.user.birthday, content['birthday'])
        self.assertEqual(self.user.is_active, content['isActive'])
        self.assertEqual(self.user.last_login, content['lastLogin'])
        self.assertEqual(list(self.user.groups.all()), content['groups'])
        self.assertEqual(list(self.user.user_permissions.all()), content['userPermissions'])

    def test_update_success(self):
        self.user.username = 'update_user'
        self.updated_user = {
            "id": self.user.id,
            "sex": self.user.sex,
            "motherTongue": self.user.mother_tongue.id,
            "username": self.user.username,
            "email": self.user.email,
            "birthday": self.user.birthday,
        }
        self.client.force_login(user=self.user)
        data = self.user.to_json()
        response = self.client.put(
            reverse('users-detail', kwargs={'pk': self.user.id}),
            data=data
        )
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.updated_user)

    def test_update_fail_as_another_user(self):
        self.user.username = 'update_user'
        self.client.logout()
        self.client.force_login(user=self.user2)
        data = self.user.to_json()

        response = self.client.put(
            reverse('users-detail', kwargs={'pk': self.user.id}),
            data=data
        )
        self.assertEqual(403, response.status_code)

    def test_update_fail_as_superuser(self):
        self.user.username = 'update_user'
        self.client.logout()
        self.client.force_login(user=self.superuser)
        data = self.user.to_json()
        response = self.client.put(
            reverse('users-detail', kwargs={'pk': self.user.id}),
            data=data
        )
        self.assertEqual(403, response.status_code)

    def test_update_unauthorized(self):
        self.client.logout()
        self.user.username = 'update_user'
        response = self.client.put(
            reverse('users-detail', kwargs={'pk': self.user.id}),
            data=self.user.to_json()
        )
        self.assertEqual(401, response.status_code)

    def test_update_password_success(self):
        data = {
            "oldPassword": "Developer1",
            "newPassword": "Developer123456"
        }
        self.client.force_login(user=self.user)
        response = self.client.put(
            reverse('users-update-password', kwargs={'pk': self.user.id}),
            data=data
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.content, b'"New password saved successfully!"')

    def test_update_password_fail_wrong_old_password(self):
        data = {
            "oldPassword": "Developer12",
            "newPassword": "Developer123456"
        }
        self.client.force_login(user=self.user)
        response = self.client.put(
            reverse('users-update-password', kwargs={'pk': self.user.id}),
            data=data
        )
        content = json.loads(response.content)
        self.assertEqual(400, response.status_code)
        self.assertEqual(content.get('oldPassword', None), ['Wrong password.'])

    def test_update_password_fail_as_superuser(self):
        data = {
            "oldPassword": "Developer1",
            "newPassword": "Developer123456"
        }
        self.client.force_login(user=self.superuser)
        response = self.client.put(
            reverse('users-update-password', kwargs={'pk': self.user.id}),
            data=data
        )
        self.assertEqual(403, response.status_code)

    def test_update_password_fail_unauthorized(self):
        data = {
            "oldPassword": "Developer1",
            "newPassword": "Developer123456"
        }
        self.client.logout()
        response = self.client.put(
            reverse('users-update-password', kwargs={'pk': self.user.id}),
            data=data
        )
        self.assertEqual(401, response.status_code)

    def test_delete_success(self):
        self.client.logout()
        self.client.force_login(user=self.user)
        response = self.client.delete(
            reverse('users-detail', kwargs={'pk': self.user.id})
        )
        self.assertEqual(204, response.status_code)

    def test_delete_fail_as_superuser(self):
        self.client.logout()
        self.client.force_login(user=self.superuser)
        response = self.client.delete(
            reverse('users-detail', kwargs={'pk': self.user.id})
        )
        self.assertEqual(403, response.status_code)

    def test_delete_unauthorized(self):
        self.client.logout()
        response = self.client.delete(
            reverse('users-detail', kwargs={'pk': self.user.id})
        )
        self.assertEqual(401, response.status_code)
