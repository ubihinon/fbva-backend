from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from users.views import UserViewSet

router = DefaultRouter()
router.register(r'', UserViewSet, base_name='users')


urlpatterns = [
    path('token-auth/', obtain_jwt_token, name='token-auth'),
]
urlpatterns += router.urls
