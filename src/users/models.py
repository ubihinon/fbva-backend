import json

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from languages.models import Language
from rest_framework.authentication import BaseAuthentication


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            username=username,
            email=self.normalize_email(email)
        )

        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username=username,
                                email=email,
                                password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


# def get_image_path(self, filename):
#     return os.path.join('photos', str(self.id), filename)


class User(AbstractBaseUser, PermissionsMixin, BaseAuthentication):
    # TODO: Delete old profile picture field
    profile_picture = models.BinaryField(blank=True, null=True)
    # profile_picture = models.ImageField(upload_to='/photos/', blank=True, null=True)
    sex = models.CharField(max_length=1, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    mother_tongue = models.ForeignKey(Language,
                                      on_delete=models.DO_NOTHING,
                                      db_column='mother_tongue',
                                      blank=True,
                                      null=True)
    username = models.CharField(
        unique=True,
        max_length=30,
        help_text='Required. 30 characters of fewer. '
                  'Letters, digits and @/./+/-/_ only.'
    )
    email = models.CharField(blank=False, null=False, max_length=40, unique=True)
    password = models.CharField(blank=False, null=False, max_length=128)
    is_active = models.BooleanField(default=True, verbose_name='Active',
                                    help_text='Designates whether this user '
                                              'should be treated as active. '
                                              'Unselect this instead of '
                                              'deleting accounts')
    is_staff = models.BooleanField(default=False, verbose_name='Staff status',
                                   help_text='Designates whether the user can '
                                             'log into this admin site.')
    is_superuser = models.BooleanField(default=False,
                                       verbose_name='Superuser status',
                                       help_text='Designates that this user '
                                                 'has all permissions without '
                                                 'explicitly assigning them.')
    last_login = models.DateTimeField(blank=True, null=True)
    date_joined = models.DateTimeField(blank=False,
                                       null=False,
                                       default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta:
        abstract = False

    @classmethod
    def authenticate(cls, username=None, password=None):
        # Check the username/password and return a User.
        user = User.objects.get(email=username)
        if user.check_password(password):
            # Get the user
            try:
                return user
            except User.DoesNotExist:
                return None

    @classmethod
    def get_user(cls, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None

    def has_perm(self, obj=None, **kwargs):
        return True

    def has_module_perms(self, app_label, **kwargs):
        return True

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.username

    def to_json(self):
        data = {
            "id": self.id,
            # "profile_picture": self.profile_picture,
            "sex": self.sex,
            # "birthday": str(self.birthday),
            "motherTongue": self.mother_tongue.id,
            "username": self.username,
            "password": self.password,
            "email": self.email,
            # "is_active": self.is_active,
            # "is_staff": self.is_staff,
            # "is_superuser": self.is_superuser,
            # "last_login": str(self.last_login),
            # "date_joined": str(self.date_joined)
        }
        return data
