import json
import sys
import urllib.parse
import dryscrape
from bs4 import BeautifulSoup
from rest_framework import views
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from images.serializers import CardImageSerializer


class CardImageListView(views.APIView):
    def get(self, request):
        if not request.GET.get('word'):
            raise NotFound('Parameter \'word\' not found')

        url = (f"https://www.google.com/search?"
               f"{urllib.parse.urlencode({'q': request.GET['word']})}"
               f"&biw=1920"
               f"&bih=976"
               f"&tbs=isz:ex,iszw:480,iszh:300"
               f"&tbm=isch"
               f"&source=lnt"
               )

        if 'linux' in sys.platform:
            dryscrape.start_xvfb()
        session = dryscrape.Session()
        session.set_attribute('auto_load_images', False)

        session.visit(url)
        response = session.body()

        parser = BeautifulSoup(response, 'lxml')

        count = int(request.GET.get('count', 100))
        links = []
        if not parser.body:
            return Response([])

        for link in parser.body.find_all(
                'div', attrs={'class': 'rg_meta notranslate'}, limit=count
        ):
            links.append({'imageUrl': json.loads(link.text).get('ou')})

        return Response(CardImageSerializer(links, many=True).data)
