from rest_framework import serializers


class CardImageSerializer(serializers.Serializer):
    imageUrl = serializers.URLField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
