from django.urls import path

from images.views import CardImageListView

urlpatterns = [
    path('', CardImageListView.as_view(), name='image-list')
]
