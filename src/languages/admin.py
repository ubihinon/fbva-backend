from django.contrib import admin

from languages.models import Language


class LanguageAdmin(admin.ModelAdmin):
    list_display = ('code', 'name',)


admin.site.register(Language, LanguageAdmin)
