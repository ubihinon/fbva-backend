import json

from django.urls import reverse

from users.models import User
from rest_framework.test import APITestCase


class LanguageCreateAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.user)

    def test_create_success(self):
        self.client.force_login(self.superuser)
        data = {
            "code": "en",
            "name": "English"
        }
        response = self.client.post(
            reverse('languages-list'),
            data=data,
        )
        data['id'] = json.loads(response.content).get('id', None)
        self.assertEqual(201, response.status_code)
        self.assertJSONEqual(response.content, data)

    def test_create_fail(self):
        self.client.logout()
        data = {
            "code": "en",
            "name": "English"
        }
        response = self.client.post(
            reverse('languages-list'),
            data=data,
        )
        self.assertEqual(401, response.status_code)

    def test_create_unauthorized(self):
        self.client.logout()
        data = {
            "code": "en",
            "name": "English"
        }
        response = self.client.post(
            reverse('languages-list'),
            data=data,
        )
        self.assertEqual(401, response.status_code)


class LanguageListAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.superuser)
        self.languages = [
            {
                "code": "en",
                "name": "English"
            },
            {
                "code": "ru",
                "name": "Russian"
            },
            {
                "code": "fr",
                "name": "French"
            }
        ]
        response = self.client.post(reverse('languages-list'), data=self.languages[0])
        self.languages[0]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('languages-list'), data=self.languages[1])
        self.languages[1]['id'] = json.loads(response.content).get('id', None)
        response = self.client.post(reverse('languages-list'), data=self.languages[2])
        self.languages[2]['id'] = json.loads(response.content).get('id', None)

    def test_get_list_success_as_user(self):
        self.client.logout()
        self.client.force_login(user=self.user)
        response = self.client.get(reverse('languages-list'))
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.languages)

    def test_get_list_fail_unauthorized(self):
        self.client.logout()
        response = self.client.get(reverse('languages-list'))
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.languages)


class LanguageDetailAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(user=self.superuser)
        self.language = {
            "code": "en",
            "name": "English"
        }
        response = self.client.post(
            reverse('languages-list'),
            data=self.language,
        )
        self.language['id'] = json.loads(response.content).get('id', None)
        self.updated_language = {
            "id": self.language['id'],
            "code": "fr",
            "name": "French"
        }

    def test_get(self):
        response = self.client.get(reverse('languages-detail', kwargs={'pk': self.language['id']}))
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.language)

    def test_update_success(self):
        response = self.client.put(
            reverse('languages-detail', kwargs={'pk': self.language['id']}),
            data=self.updated_language
        )
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.updated_language)

    def test_update_fail(self):
        self.client.logout()
        self.client.login(user=self.user)
        response = self.client.put(
            reverse('languages-detail', kwargs={'pk': self.language['id']}),
            data=self.updated_language
        )
        self.assertEqual(401, response.status_code)

    def test_update_unauthorized(self):
        self.client.logout()
        response = self.client.put(
            reverse('languages-detail', kwargs={'pk': self.language['id']}),
            data=self.updated_language
        )
        self.assertEqual(401, response.status_code)

    def test_delete_success(self):
        response = self.client.delete(
            reverse('languages-detail', kwargs={'pk': self.language['id']})
        )
        self.assertEqual(204, response.status_code)

    def test_delete_fail(self):
        self.client.logout()
        self.client.login(user=self.user)
        response = self.client.delete(
            reverse('languages-detail', kwargs={'pk': self.language['id']})
        )
        self.assertEqual(401, response.status_code)

    def test_delete_unauthorized(self):
        self.client.logout()
        response = self.client.delete(
            reverse('languages-detail', kwargs={'pk': self.language['id']})
        )
        self.assertEqual(401, response.status_code)
