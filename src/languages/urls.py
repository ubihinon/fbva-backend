from rest_framework.routers import DefaultRouter

from languages.views import LanguageViewSet

router = DefaultRouter()
router.register(r'', LanguageViewSet, base_name='languages')

urlpatterns = router.urls
