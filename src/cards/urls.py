from rest_framework.routers import DefaultRouter

from cards.views import CardViewSet

router = DefaultRouter()
router.register(r'', CardViewSet, base_name='cards')

urlpatterns = router.urls
