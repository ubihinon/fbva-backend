import boto3

from fbva.settings import AWS_UPLOAD_REGION, AWS_UPLOAD_ACCESS_KEY_ID, AWS_UPLOAD_SECRET_KEY


def get_s3_client():
    return boto3.client(
        's3',
        config=boto3.session.Config(signature_version='s3v4'),
        region_name=AWS_UPLOAD_REGION,
        aws_access_key_id=AWS_UPLOAD_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_UPLOAD_SECRET_KEY,
    )
