# Generated by Django 2.0.2 on 2018-05-08 01:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cards', '0001_initial'),
        ('intervalcard', '0001_initial'),
        ('tags', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('languages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='card_creator',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='card',
            name='fork_card',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='cards.Card'),
        ),
        migrations.AddField(
            model_name='card',
            name='interval_card',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='intervalcard.IntervalCard'),
        ),
        migrations.AddField(
            model_name='card',
            name='source_language',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='source_language_id', to='languages.Language'),
        ),
        migrations.AddField(
            model_name='card',
            name='tags',
            field=models.ManyToManyField(to='tags.Tag'),
        ),
        migrations.AddField(
            model_name='card',
            name='target_language',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='target_language_id', to='languages.Language'),
        ),
    ]
