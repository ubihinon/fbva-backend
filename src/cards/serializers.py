from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from cards.models import Card
from core.common.serializers import ObjectPrimaryKeyRelatedField
from intervalcard.serializers import IntervalCardSerializer, IntervalCardUpdateSerializer
from languages.models import Language
from languages.serializers import LanguageSerializer
from tags.models import Tag
from tags.serializers import TagSerializer

I_KNOW_IT_CHOICES = (
    ('no', 'no'),
    ('know', 'know'),
)


class CardIntervalCardUpdateSerializer(ModelSerializer):
    intervalCard = IntervalCardUpdateSerializer(source='interval_card')

    def update(self, instance, validated_data):
        return Card.objects.get(id=self.context.get('card_id'))

    class Meta:
        model = Card
        fields = ('intervalCard',)


class CardSerializer(ModelSerializer):
    tags = TagSerializer(many=True)
    iKnowIt = serializers.ChoiceField(source='i_know_it', choices=I_KNOW_IT_CHOICES,
                                      default=I_KNOW_IT_CHOICES[0])
    forkCard = serializers.PrimaryKeyRelatedField(source='fork_card', queryset=Card.objects.all())
    sourceLanguage = LanguageSerializer(source='source_language')
    targetLanguage = LanguageSerializer(source='target_language')
    pronunciationPath = serializers.SerializerMethodField(source='pronunciation_path',
                                                          method_name='get_pronunciation_path')
    imagePath = serializers.SerializerMethodField(source='image_path',
                                                  method_name='get_image_path')
    intervalCard = IntervalCardSerializer(source='interval_card')

    class Meta:
        model = Card
        fields = (
            'id',
            'rating',
            'iKnowIt',
            'word',
            'forkCard',
            'tags',
            'sourceLanguage',
            'targetLanguage',
            'pronunciationPath',
            'imagePath',
            'intervalCard'
        )

    def get_pronunciation_path(self, obj):
        return obj.get_download_pronunciation_url()

    def get_image_path(self, obj):
        return obj.get_download_image_url()


class CardCreateSerializer(ModelSerializer):
    tags = ObjectPrimaryKeyRelatedField(many=True, queryset=Tag.objects.all(),
                                        write_only=True, allow_null=True)
    iKnowIt = serializers.ChoiceField(source='i_know_it', required=False, choices=I_KNOW_IT_CHOICES,
                                      default=I_KNOW_IT_CHOICES[0])
    forkCard = serializers.PrimaryKeyRelatedField(source='fork_card',
                                                  queryset=Card.objects.all(),
                                                  required=False,
                                                  allow_null=True)
    sourceLanguage = ObjectPrimaryKeyRelatedField(source='source_language',
                                                  queryset=Language.objects.all())
    targetLanguage = ObjectPrimaryKeyRelatedField(source='target_language',
                                                  queryset=Language.objects.all())
    imagePath = serializers.URLField(source='image_path',
                                     required=False,
                                     allow_null=True,
                                     allow_blank=True)

    def to_representation(self, instance):
        return CardSerializer(instance).data

    def create(self, validated_data):
        tags = validated_data.pop('tags')
        card = Card.objects.create(**validated_data)
        card.tags.set(tags)
        return card

    def update(self, instance, validated_data):
        instance.rating = validated_data.get('rating', instance.rating)
        instance.i_know_it = validated_data.get('i_know_it', instance.i_know_it)
        instance.word = validated_data.get('word', instance.word)
        instance.fork_card = validated_data.get('fork_card', instance.fork_card)
        instance.card_creator = validated_data.get('card_creator', instance.card_creator)
        instance.tags.set([int(tag_id.id) for tag_id in validated_data.get('tags', instance.tags)])
        instance.source_language = validated_data.get('source_language', instance.source_language)
        instance.target_language = validated_data.get('target_language', instance.target_language)
        instance.pronunciation_path = validated_data.get(
            'pronunciation_path', instance.pronunciation_path
        )
        instance.image_path = validated_data.get('image_path', instance.image_path)
        instance.interval_card = validated_data.get('interval_card', instance.interval_card)
        instance.add_english_pronunciation()
        instance.upload_image()
        return instance

    class Meta:
        model = Card
        fields = (
            'id',
            'iKnowIt',
            'word',
            'forkCard',
            'tags',
            'sourceLanguage',
            'targetLanguage',
            'imagePath',
        )
