import copy
import json
from datetime import datetime
import datetime

from django.urls import reverse

from intervals.models import Interval
from languages.models import Language
from users.models import User
from rest_framework.test import APITestCase


def create_languages():
    return (
        Language.objects.create(code='en', name='English'),
        Language.objects.create(code='ru', name='Russian'),
        Language.objects.create(code='fr', name='French')
    )


def create_intervals():
    Interval.objects.create(order=1, interval="00:00:30")
    Interval.objects.create(order=2, interval="00:01:00")
    Interval.objects.create(order=3, interval="00:02:00")


class CardCreateAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.superuser)

        self.languages = create_languages()
        create_intervals()

    def test_create_success(self):
        self.client.force_login(self.user)
        data = {
            "iKnowIt": "no",
            "word": json.dumps({'word': 'word1', 'translations': [{'pos': 'noun', 'text': 'tr1'}]}),
            "tags": [],
            "sourceLanguage": self.languages[0].id,
            "targetLanguage": self.languages[1].id
        }
        expected = {
            "id": 73,
            "rating": 0,
            "iKnowIt": 'no',
            "word": {"word": "word1", "translations": [{"pos": "noun", "text": "tr1"}]},
            "cardCreator": self.user.id,
            "tags": [],
            "sourceLanguage": {
                "id": self.languages[0].id,
                "code": "en",
                "name": "English"
            },
            "targetLanguage": {
                "id": self.languages[1].id,
                "code": "ru",
                "name": "Russian"
            },
            "pronunciationPath": 'https://\\w+',
            "imagePath": 'https://\\w+',
            "intervalCard": {
                "lastShown": None,
                "nextShow": None
            }
        }
        response = self.client.post(
            reverse('cards-list'),
            data=data,
        )
        content = json.loads(response.content)
        data['id'] = content.get('id', None)
        self.assertEqual(201, response.status_code)
        self.assertEqual(content.get('rating'), expected['rating'])
        self.assertEqual(content.get('word'), expected['word'])
        self.assertEqual(content.get('forkCard'), None)
        self.assertEqual(content.get('tags'), expected['tags'])
        self.assertEqual(content.get('sourceLanguage'), expected['sourceLanguage'])
        self.assertEqual(content.get('targetLanguage'), expected['targetLanguage'])
        self.assertEqual(content.get('intervalCard'), expected['intervalCard'])
        self.assertRegex(content.get('pronunciationPath'), 'https://\\w+')

    def test_create_fail_unauthorized(self):
        self.client.logout()
        data = {
            "iKnowIt": "no",
            "word": json.dumps({'word': 'word1', 'translations': [{'pos': 'noun', 'text': 'tr1'}]}),
            "tags": [],
            "sourceLanguage": self.languages[0].id,
            "targetLanguage": self.languages[1].id
        }
        response = self.client.post(
            reverse('cards-list'),
            data=data,
        )
        self.assertEqual(401, response.status_code)


class CardListAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.superuser)

        self.languages = create_languages()
        create_intervals()

        card1 = {
            "iKnowIt": "no",
            "word": json.dumps({'word': 'word1', 'translations': [{'pos': 'noun', 'text': 'tr1'}]}),
            "tags": [],
            "sourceLanguage": self.languages[0].id,
            "targetLanguage": self.languages[1].id
        }
        card2 = {
            "iKnowIt": "no",
            "word": json.dumps({'word': 'word2', 'translations': [{'pos': 'noun', 'text': 'tr2'}]}),
            "tags": [],
            "sourceLanguage": self.languages[0].id,
            "targetLanguage": self.languages[1].id
        }
        self.response1 = self.client.post(
            reverse('cards-list'),
            data=card1,
        )

        self.response2 = self.client.post(
            reverse('cards-list'),
            data=card2,
        )
        self.card1 = json.loads(self.response1.content)
        self.card2 = json.loads(self.response2.content)

    def test_get_list(self):
        response = self.client.get(reverse('cards-list'))

        self.assertEqual(200, response.status_code)
        self.assertEqual(len(response.data), 2)

    def test_get_list_fail_unauthorized(self):
        self.client.logout()
        response = self.client.get(reverse('cards-list'))

        self.assertEqual(401, response.status_code)


class CardDetailAPITestCase(APITestCase):

    def setUp(self):
        self.user1 = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.user2 = User.objects.create_user('user2', 'user2@mail.com', 'user2')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')

        self.client.force_login(self.superuser)

        self.languages = create_languages()
        create_intervals()

        self.client.force_login(self.user1)
        data = {
            "iKnowIt": "no",
            "word": json.dumps({'word': 'word1', 'translations': [{'pos': 'noun', 'text': 'tr1'}]}),
            "tags": [],
            "sourceLanguage": self.languages[0].id,
            "targetLanguage": self.languages[1].id
        }
        expected = {
            "id": 73,
            "rating": 0,
            "iKnowIt": 'no',
            "word":
                "{\"word\": \"word1\", \"translations\": [{\"pos\": \"noun\", \"text\": \"tr1\"}]}",
            "cardCreator": self.user1.id,
            "tags": [],
            "sourceLanguage": {
                "id": self.languages[0].id,
                "code": "en",
                "name": "English"
            },
            "targetLanguage": {
                "id": self.languages[1].id,
                "code": "ru",
                "name": "Russian"
            },
            "pronunciationPath": 'https://\\w+',
            "imagePath": 'https://\\w+',
            "intervalCard": {
                "lastShown": None,
                "nextShow": None
            }
        }
        response = self.client.post(
            reverse('cards-list'),
            data=data,
        )
        self.card = json.loads(response.content)

    def test_get(self):
        response = self.client.get(reverse('cards-detail', kwargs={'pk': self.card['id']}))
        self.assertEqual(200, response.status_code)
        self.assertJSONEqual(response.content, self.card)

    def test_update_success(self):
        self.updated_card = self.card
        self.updated_card['word'] = {
            "word": "word123", "translations": [{"pos": "noun", "text": "tr2"}]
        }

        response = self.client.put(
            reverse('cards-detail', kwargs={'pk': self.card['id']}),
            data=self.updated_card,
            format='json'
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data['word'], self.updated_card['word'])

    def test_update_fail(self):
        self.client.logout()
        self.client.login(user=self.user2)

        self.updated_card = self.card
        self.updated_card['word'] = {
            "word": "word123", "translations": [{"pos": "noun", "text": "tr2"}]
        }

        response = self.client.put(
            reverse('cards-detail', kwargs={'pk': self.card['id']}),
            data=self.updated_card,
            format='json'
        )
        self.assertEqual(401, response.status_code)

    def test_update_fail_unauthorized(self):
        self.client.logout()

        self.updated_card = self.card
        self.updated_card['word'] = {
            "word": "word123", "translations": [{"pos": "noun", "text": "tr2"}]
        }

        response = self.client.put(
            reverse('cards-detail', kwargs={'pk': self.card['id']}),
            data=self.updated_card,
            format='json'
        )
        self.assertEqual(401, response.status_code)

    def test_delete_success(self):
        response = self.client.delete(
            reverse('cards-detail', kwargs={'pk': self.card['id']})
        )
        self.assertEqual(204, response.status_code)

    def test_delete_fail_another_user(self):
        self.client.logout()
        self.client.login(user=self.user2)
        response = self.client.delete(
            reverse('cards-detail', kwargs={'pk': self.card['id']})
        )
        self.assertEqual(401, response.status_code)

    def test_delete_unauthorized(self):
        self.client.logout()
        response = self.client.delete(
            reverse('cards-detail', kwargs={'pk': self.card['id']})
        )
        self.assertEqual(401, response.status_code)


class IntervalCardUpdateViewAPITestView(APITestCase):

    def setUp(self):
        self.user1 = User.objects.create_user('user1', 'user1@mail.com', 'user1')
        self.user2 = User.objects.create_user('user2', 'user2@mail.com', 'user2')
        self.superuser = User.objects.create_superuser('admin', 'admin@mail.com', 'admin')
        self.client.force_login(self.superuser)

        self.languages = create_languages()
        create_intervals()

        self.client.force_login(self.user1)
        data = {
            "iKnowIt": "no",
            "word": json.dumps({'word': 'word1', 'translations': [{'pos': 'noun', 'text': 'tr1'}]}),
            "tags": [],
            "sourceLanguage": self.languages[0].id,
            "targetLanguage": self.languages[1].id
        }
        response = self.client.post(
            reverse('cards-list'),
            data=data
        )
        self.card = json.loads(response.content)

    def test_update(self):
        updated_card = copy.deepcopy(self.card)
        updated_card['intervalCard']['lastShown'] = f"{datetime.datetime.now():%Y-%m-%d %H:%M:%S}"
        response = self.client.put(
            reverse('interval-card-update', kwargs={'pk': updated_card['id']}),
            data=updated_card,
            format='json'
        )
        self.assertEqual(200, response.status_code)
        expected = datetime.datetime.strptime(
            updated_card['intervalCard']['lastShown'],
            '%Y-%m-%d %H:%M:%S'
        ) + datetime.timedelta(minutes=1)
        self.assertEqual(
            datetime.datetime.strptime(
                str(response.data['intervalCard']['nextShow'])[:19],
                '%Y-%m-%d %H:%M:%S'
            ),
            expected
        )
