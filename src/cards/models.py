import json
import requests
from django.contrib.postgres.fields import JSONField
from django.db import models

from cards.pronunciation import PronunciationTTS
from cards.utils import get_s3_client
from fbva.settings import AWS_UPLOAD_BUCKET
from intervalcard.models import IntervalCard
from languages.models import Language
from tags.models import Tag
from users.models import User


class Card(models.Model):
    rating = models.FloatField(default=0)
    i_know_it = models.TextField(blank=True, null=True)
    word = JSONField(blank=False, null=False, default=dict)
    fork_card = models.ForeignKey('self', on_delete=models.DO_NOTHING,
                                  blank=True, null=True)
    card_creator = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    tags = models.ManyToManyField(Tag)
    source_language = models.ForeignKey(
        Language,
        related_name='source_language_id',
        on_delete=models.CASCADE
    )
    target_language = models.ForeignKey(
        Language,
        related_name='target_language_id',
        on_delete=models.CASCADE
    )
    pronunciation_path = models.FileField(upload_to='pronunciations', null=True)
    image_path = models.URLField(null=True, blank=True, max_length=1000)
    interval_card = models.OneToOneField(IntervalCard,
                                         on_delete=models.CASCADE,
                                         null=True,
                                         blank=True)

    def add_pronunciation(self):
        try:
            word = self.word['word']
        except TypeError:
            word = json.loads(self.word)['word']
        if word is None:
            raise ValueError('Word name doesn\'t exists')
        tts = PronunciationTTS(word)
        tts.set_card(self)
        return tts.save_pronunciation()

    def add_english_pronunciation(self):
        if self.source_language.code != 'en':
            return False
        try:
            word = self.word['word']
        except TypeError:
            word = json.loads(self.word)['word']
        if word is None:
            raise ValueError('Word name doesn\'t exists')

        sound_data = requests.get(
            'http://dict.youdao.com/dictvoice?audio={}&type=1'.format(word),
            stream=True
        )
        if sound_data.status_code == 200:
            file_name = '{}.mp3'.format(self.id)
            path = 'pronunciations/{}/{}'.format(self.card_creator.id, file_name)
            get_s3_client().put_object(
                Key=path,
                Body=sound_data.content,
                Bucket=AWS_UPLOAD_BUCKET
            )
            self.pronunciation_path.name = path
            self.save()
            return True

    def get_download_pronunciation_url(self):
        if self.pronunciation_path.name:
            return get_s3_client().generate_presigned_url(
                ClientMethod='get_object',
                Params={
                    'Bucket': AWS_UPLOAD_BUCKET,
                    'Key': self.pronunciation_path.name
                }
            )
        return None

    def delete_pronunciation(self):
        if self.pronunciation_path.name:
            get_s3_client().delete_object(
                Key=self.pronunciation_path.name,
                Bucket=AWS_UPLOAD_BUCKET
            )

    def upload_image(self):
        if self.image_path:
            image_data = requests.get(
                self.image_path,
                stream=True
            )
            if image_data.status_code == 200:
                file_name = '{}.png'.format(self.id)
                path = 'images/{}/{}'.format(self.card_creator.id, file_name)
                get_s3_client().put_object(
                    Key=path,
                    Body=image_data.content,
                    Bucket=AWS_UPLOAD_BUCKET
                )
                self.image_path = path
                self.save()
                return True
        return None

    def get_download_image_url(self):
        if self.image_path:
            return get_s3_client().generate_presigned_url(
                ClientMethod='get_object',
                Params={
                    'Bucket': AWS_UPLOAD_BUCKET,
                    'Key': self.image_path
                }
            )
        return None

    def delete_image(self):
        if self.image_path:
            get_s3_client().delete_object(
                Key=self.image_path,
                Bucket=AWS_UPLOAD_BUCKET
            )

    def __str__(self):
        return self.word.__str__()
