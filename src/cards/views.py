from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from cards.models import Card
from cards.permissions import IsOwnerOrReadOnly
from cards.serializers import CardSerializer, CardCreateSerializer, \
    CardIntervalCardUpdateSerializer


class CardViewSet(viewsets.ModelViewSet):
    queryset = Card.objects.all()
    serializer_class = CardSerializer
    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('id',)

    def get_queryset(self):
        is_new = self.request.GET.get('words_to_learn')

        cards = Card.objects.filter(card_creator=self.request.user)

        if is_new:
            cards = Card.objects.raw('''
                    SELECT c.* FROM cards_card AS c 
                    INNER JOIN intervalcard_intervalcard AS ic ON ic.id=c.interval_card_id
                    INNER JOIN intervals_interval AS i ON i.id=ic.interval_id
                    INNER JOIN fbva."public"."users_user" AS u ON u.id=c.card_creator_id
                    WHERE ((ic.last_shown::TIMESTAMP + i.interval::INTERVAL) <= LOCALTIMESTAMP 
                    OR last_shown IS NULL) AND u.id = {}
                    ORDER BY last_shown
                    '''.format(self.request.user.id))
        return cards

    def get_permissions(self):
        if self.action in ('list', 'retrieve', 'update', 'delete', 'interval',
                           'bulk_delete_cards'):
            self.permission_classes = (IsOwnerOrReadOnly,)
        elif self.action in ('create',):
            self.permission_classes = (IsAuthenticated,)
        return super().get_permissions()

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return CardSerializer
        elif self.action in ('interval',):
            return CardIntervalCardUpdateSerializer
        return CardCreateSerializer

    @action(detail=False, methods=['DELETE'], serializer_class=CardCreateSerializer)
    def bulk_delete_cards(self, request, *args, **kwargs):
        card_ids = self.request.GET.getlist('card')
        if card_ids:
            cards = Card.objects.filter(pk__in=[int(i) for i in card_ids])
            for card in cards:
                card.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['PUT', 'GET'], serializer_class=CardIntervalCardUpdateSerializer)
    def interval(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()
        return Response(serializer.data)

    def get_serializer_context(self):
        if 'pk' in self.kwargs:
            return {'card_id': self.kwargs['pk']}
        return {}

    def perform_create(self, serializer):
        serializer.save(card_creator=self.request.user)
        serializer.save()
