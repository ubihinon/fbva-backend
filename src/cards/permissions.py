from django.views import View
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.request import Request


class IsOwnerOrReadOnly(BasePermission):
    def has_permission(self, request: Request, view: View):
        return IsAuthenticated().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        return obj.card_creator == request.user
