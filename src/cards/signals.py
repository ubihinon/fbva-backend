from django.db.models import signals
from django.db.models.signals import post_save, pre_delete, post_delete
from django.dispatch import receiver

from cards.models import Card
from intervalcard.models import IntervalCard
from intervals.models import Interval

DISPATCH_UID = 'dispatch_uid_string'


@receiver(post_save, sender=Card)
def save_interval_card(sender, instance, created=False, **kwargs):
    if created:
        instance.interval_card = IntervalCard.objects.create(interval=Interval.objects.get(order=1))
        instance.save()


@receiver(pre_delete, sender=Card, dispatch_uid=DISPATCH_UID)
def delete_interval_card(sender, instance, **kwargs):
    interval_card = IntervalCard.objects.get(id=instance.interval_card.id)
    if interval_card:
        signals.pre_delete.disconnect(delete_interval_card, sender=Card, dispatch_uid=DISPATCH_UID)
        interval_card.delete()
    signals.pre_delete.connect(delete_interval_card, sender=Card, dispatch_uid=DISPATCH_UID)


@receiver(post_save, sender=Card)
def save_word_pronunciation(sender, instance, created=False, **kwargs):
    if created:
        instance.add_english_pronunciation()
        instance.upload_image()


@receiver(post_delete, sender=Card)
def delete_pronunciation(sender, instance, using, **kwargs):
    instance.delete_pronunciation()
    instance.delete_image()
