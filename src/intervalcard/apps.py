from django.apps import AppConfig


class IntervalCardConfig(AppConfig):
    name = 'intervalcard'
