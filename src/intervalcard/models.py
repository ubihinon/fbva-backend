from django.db import models

from intervals.models import Interval


class IntervalCard(models.Model):
    interval = models.ForeignKey(Interval, on_delete=models.CASCADE)
    last_shown = models.DateTimeField(blank=True, null=True, default=None)

    def __str__(self):
        return 'interval: {}, last shown: {}'.format(self.interval.interval, self.last_shown)
