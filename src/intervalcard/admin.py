from django.contrib import admin

from intervalcard.models import IntervalCard


class IntervalCardAdmin(admin.ModelAdmin):
    pass


admin.site.register(IntervalCard, IntervalCardAdmin)
