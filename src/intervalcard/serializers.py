from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from cards.models import Card
from intervalcard.models import IntervalCard
from intervals.models import Interval


class IntervalCardSerializer(serializers.ModelSerializer):
    lastShown = serializers.DateTimeField(source='last_shown')
    nextShow = serializers.DateTimeField(source='next_show', required=False)

    def to_representation(self, instance):
        next_show = None
        if instance.last_shown:
            next_show = instance.last_shown + instance.interval.interval
        return {
            'lastShown': instance.last_shown,
            'nextShow': next_show
        }

    class Meta:
        model = IntervalCard
        fields = ('lastShown', 'nextShow')


class IntervalCardUpdateSerializer(serializers.ModelSerializer):
    lastShown = serializers.DateTimeField(source='last_shown')
    nextShow = serializers.DateTimeField(source='next_show', required=False)

    class Meta:
        model = IntervalCard
        fields = ('lastShown', 'nextShow')

    def to_representation(self, value):
        return IntervalCardSerializer(value).data

    def to_internal_value(self, data):
        serializer = IntervalCardSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        card = Card.objects.get(id=self.context.get('card_id'))
        try:
            interval = Interval.objects.get(order=card.interval_card.interval.order + 1)
        except ObjectDoesNotExist:
            interval = card.interval_card.interval

        card.interval_card.last_shown = serializer.validated_data['last_shown']
        card.interval_card.interval = interval
        card.interval_card.save()
        return card.interval_card
