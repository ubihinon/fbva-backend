# This script is needed for gitlab CI
# It waits until postgres is up and ready to process queries

import os
import time

import dotenv
import psycopg2

if not os.environ.get('POSTGRES_USER'):
    dotenv.read_dotenv(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'src/fbva/.env_dev'))


def is_postgress_ready(host, user, password, dbname, port=5432):
    c = None
    conn = None
    try:
        conn = psycopg2.connect(
            host=host,
            port=port,
            user=user,
            dbname=dbname,
            password=password
        )
        c = conn.cursor()
        c.execute('SELECT 1')
        c.fetchone()
    except psycopg2.OperationalError as e:
        print(e)
        return False
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()
    return True


if __name__ == '__main__':
    while True:
        pg_ready = is_postgress_ready(
            host=os.environ.get('POSTGRES_HOST'),
            port=os.environ.get('POSTGRES_PORT'),
            user=os.environ.get('POSTGRES_USER'),
            dbname=os.environ.get('POSTGRES_DB'),
            password=os.environ.get('POSTGRES_PASSWORD'),
        )
        if pg_ready:
            exit(0)
        time.sleep(1)
