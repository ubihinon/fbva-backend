FROM ubuntu

ENV DJANGO_SETTINGS_MODULE=fbva.settings
ENV SECRET_KEY='02s4z#j82jwj8hktn88z@656dh=sj)+bylh=xifc5ih(wa2=xm'
ENV POSTGRES_DB=fbva
ENV POSTGRES_USER=fbva
#ENV POSTGRES_PASSWORD=111111
#ENV POSTGRES_HOST=localhost
#ENV POSTGRES_PORT=5427
ENV POSTGRES_PASSWORD=5key2Dge
ENV POSTGRES_HOST=fbva.cxrft7lqmqmi.us-east-1.rds.amazonaws.com
ENV POSTGRES_PORT=5432
ENV AWS_UPLOAD_REGION=eu-west-2
ENV AWS_UPLOAD_BUCKET=vocabulary-plus
ENV AWS_UPLOAD_USERNAME=vocabulary-plus
ENV AWS_UPLOAD_ACCESS_KEY_ID=AKIAJTCPAZDMKRUJ6SDA
ENV AWS_UPLOAD_SECRET_KEY='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'

RUN mkdir -p /var/www
COPY . /var/www

WORKDIR /var/www

# sudo docker build -t fbva_web .
# sudo docker run -d --restart always --net=host --name fbva_web fbva_web

# aws ecr get-login --no-include-email
# sudo docker tag fbva_web:latest 276460509780.dkr.ecr.us-east-1.amazonaws.com/fbva_web
# sudo docker push 276460509780.dkr.ecr.us-east-1.amazonaws.com/fbva_web:latest

RUN apt-get update
RUN apt-get install -y sudo

RUN sudo apt-get install -y python3.6 \
                            python3-pip \
                            qt5-default \
                            libqt5webkit5-dev \
                            build-essential \
                            python-lxml \
                            xvfb \
                            software-properties-common

RUN apt-get update
RUN sudo add-apt-repository -y ppa:nginx/stable
RUN sudo apt-get install -y nginx

RUN sudo apt-get install -y

RUN pip3 install -r config/requirements.txt --no-cache-dir


COPY config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY config/uwsgi.ini /opt/conf/uwsgi.ini
COPY config/uwsgi_params /opt/conf/uwsgi_params

CMD ["/bin/bash", "-c", "config/docker-entrypoint.sh"]

EXPOSE 80
