#!/bin/bash

python /app/wait_for_postgres.py
cd /app/src

python3 manage.py migrate

python3 manage.py loaddata ./fixtures/language.json
python3 manage.py loaddata ./fixtures/interval.json
python3 manage.py loaddata ./fixtures/setting.json
#python3 manage.py loaddata ./fixtures/card.json

gunicorn fbva.wsgi -b 0.0.0.0:8000 --reload