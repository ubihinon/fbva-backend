#!/bin/bash

sudo docker run --name fbva_postgres --restart always -e POSTGRES_PASSWORD=111111 -e POSTGRES_DB=fbva -e POSTGRES_USER=fbva -p 5427:5432 -d postgres


python3 wait_for_postgres.py
python3 src/manage.py migrate
python3 src/manage.py initadmin

python3 src/manage.py loaddata src/fixtures/language.json
python3 src/manage.py loaddata src/fixtures/interval.json
python3 src/manage.py loaddata src/fixtures/setting.json
