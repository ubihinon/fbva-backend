#!/bin/env bash
psql -U postgres -c "CREATE USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD'"
psql -U postgres -c "CREATE DATABASE $POSTGRES_DB OWNER $POSTGRES_USER"
psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER;"
#psql -U postgres -c "CREATE USER fbva WITH PASSWORD '111111'"
#psql -U postgres -c "CREATE DATABASE fbva OWNER fbva"
#psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE fbva TO fbva;"
# sudo -u postgres psql
