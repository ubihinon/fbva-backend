#!/bin/bash

/etc/init.d/nginx start

cd src

python3 manage.py migrate
python3 manage.py initadmin
python3 manage.py collectstatic --no-input

python3 manage.py loaddata ./fixtures/language.json
python3 manage.py loaddata ./fixtures/interval.json
python3 manage.py loaddata ./fixtures/setting.json

uwsgi --ini /opt/conf/uwsgi.ini
